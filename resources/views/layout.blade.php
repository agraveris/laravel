<!DOCTYPE html>
<html lang="en">
<head>
      @include('head')
      @yield ('head')
</head>
<body>
   <header>
      @include('navigation')
      @yield ('navigation')
   </header>

   <main class="main">  
      <div class="main__left">
         @yield ('content')
      </div>
      </div>
      <div class="main__right">
         <img src="images/illustration.jpg" alt="Animated visual representation of digital marketing" class="logo">
      </div>
   </main>
</body>
</html>