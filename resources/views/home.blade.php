@extends ('layout')

@section ('content')
   <h1 class="main__title">DIGITAL</h1>
   <h2 class="main__subtitle">MARKETING</h2>
   <p class="main__text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores incidunt distinctio suscipit, est nam commodi consectetur animi cupiditate? Soluta, ea.</p>
   <a href="/about" class="btn btn--secondary">LEARN MORE</a>
@endsection