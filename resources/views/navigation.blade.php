@section ('navigation')
   <nav class="nav">
      <div class="nav__other">
         <a href="#" class="nav__logo">LOGO</a>
         <a href="#" class="nav__hamburger" onclick="toggleNav()">
            <i class="fas fa-bars"></i>
         </a>
      </div>
      <ul class="nav__list" id="nav__list-js">
         <li class="nav__list-item"><a href="/home" class="nav__link">Home</a></li>
         <li class="nav__list-item"><a href="services" class="nav__link">Services</a></li>
         <li class="nav__list-item"><a href="/about" class="nav__link">About</a></li>
         <li class="nav__list-item"><a href="/contact" class="nav__link">Contact</a></li>
         <li class="nav__list-item"><a href="/faq" class="nav__link">FAQ</a></li>
         <li class="nav__list-item"><a href="/sign-up" class="nav__link btn btn--primary">SIGN UP</a></li>
      </ul>
   </nav>
@endsection