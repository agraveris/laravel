@extends ('layout')

@section('content')

   <h1 class="main__title">SIGN UP</h1>
   <h1 class="main__subtitle">Enter your data</h1>
   <p class="submit__message">{{ session('message')}}</p>

   <form action="/sign-up" class="main__form" method="POST">
      @csrf
      <label for="name" class="main__form-label">name:</label>
      <input type="text" class="main__form-input" id="main__form-name" name="name">
      <label for="email" class="main__form-label">email:</label>
      <input type="email" class="main__form-input" id="main__form-email" name="email">
      <input type="submit" value="SIGN UP" class="btn btn--primary">
   </form>
@endsection