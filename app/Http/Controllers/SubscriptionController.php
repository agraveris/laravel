<?php

namespace App\Http\Controllers;
use App\Subscriptions;

use Illuminate\Http\Request;

class SubscriptionController extends Controller {

   public function index() {
      return view('signup');
   }

   public function store() {

      if ((request('name')&&request('email'))) {

         $subscriber = new Subscriptions();
   
         $subscriber->name = request('name');
         $subscriber->email = request('email');
   
         $subscriber->save();
   
         return redirect('/sign-up')->with('message', 'Thank you for subscribing!');
      } else {
         return redirect('/sign-up')->with('message', 'Please fill out the form!');
      }
   }
}