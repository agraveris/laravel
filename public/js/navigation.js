/* Toggle between showing and hiding the navigation 
menu links when the user clicks on 
the hamburger menu / bar icon */
function toggleNav() {
   let navList = document.getElementById("nav__list-js");

   if (window.getComputedStyle(navList).display === "block") {
     navList.style.display = "none";
   } else {
     navList.style.display = "block";
   }
}